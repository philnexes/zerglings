package Sample;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class HiveMindEmulator
{
    private static HiveMindEmulator instance = null;
    private static ImageView imv = new ImageView(new Image("hiveMind.png"));

    protected HiveMindEmulator()
    {
        imv.setFitWidth(70);
        imv.setPreserveRatio(true);
        imv.setSmooth(true);
        imv.setCache(true);
    }

    public static HiveMindEmulator getInstance()
    {
        if(instance == null)
            instance = new HiveMindEmulator();

        return instance;
    }

    public HiveMindEmulator setPosition(double x, double y)
    {
        imv.setX(x);
        imv.setY(y);
        return this;
    }

    public HiveMindEmulator paint(Pane root)
    {
        if(!root.getChildren().contains(this.imv))
            root.getChildren().add(this.imv);

        return this;
    }

    public double getX()
    {
        return this.imv.getX();
    }

    public double getY()
    {
        return this.imv.getY();
    }

    public double getWidth()
    {
        return this.imv.getFitWidth();
    }

    public double getHeight()
    {
        return this.imv.getFitHeight();
    }

    public static boolean exists()
    {
        if(instance == null)
            return false;
        else
            return true;
    }
}
