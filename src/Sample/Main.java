package Sample;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.ImagePattern;
import javafx.stage.Stage;
import javafx.scene.input.MouseEvent;

import java.io.File;
import java.net.URL;


public class Main extends Application
{
    //region Variables
    Pane root;
    Scene scene;
    static int width = 1000;
    static int length = 1000;
    static int zerg_count = 70;
    //endregion

    @Override
    public void start(Stage window) throws Exception
    {
        //region Initialization
        MediaPlayer mediaPlayer = new MediaPlayer(new Media(getClass().getResource("/starcraft.mp3").toURI().toString()));
        root = new Pane();
        scene = new Scene(root, width, length);
        scene.setFill(new ImagePattern(new Image("creep.jpg")));
        window.setTitle("Zerglings by PIN0068");
        window.setScene(scene);
        window.show();
        mediaPlayer.play();
        mediaPlayer.setVolume(0.1);

        //endregion

        Swarm swarm = new Swarm(zerg_count);
        swarm.paint(root);
        swarm.moveZerglings();

        scene.addEventFilter(MouseEvent.MOUSE_PRESSED, (MouseEvent mouseEvent) ->
        {
            double mouseX = mouseEvent.getX();
            double mouseY = mouseEvent.getY();

            if(mouseEvent.isPrimaryButtonDown())
            {
                double diffX = HiveMindEmulator.getInstance().getWidth()/2;
                double diffY = HiveMindEmulator.getInstance().getHeight();
                HiveMindEmulator.getInstance().setPosition( mouseX - diffX, mouseY + diffY).paint(root);
            }

            if(mouseEvent.isMiddleButtonDown())
            {
                swarm.addZergling(mouseX, mouseY, root);
            }
        });

        scene.setOnKeyPressed(event ->
        {

            if(HiveMindEmulator.exists())
            {
                if(event.getCode() == KeyCode.RIGHT)
                    HiveMindEmulator.getInstance().setPosition(HiveMindEmulator.getInstance().getX() + 8, HiveMindEmulator.getInstance().getY());

                if(event.getCode() == KeyCode.UP)
                    HiveMindEmulator.getInstance().setPosition(HiveMindEmulator.getInstance().getX(), HiveMindEmulator.getInstance().getY() - 8);

                if(event.getCode() == KeyCode.DOWN)
                    HiveMindEmulator.getInstance().setPosition(HiveMindEmulator.getInstance().getX(), HiveMindEmulator.getInstance().getY() + 8);

                if(event.getCode() == KeyCode.LEFT)
                    HiveMindEmulator.getInstance().setPosition(HiveMindEmulator.getInstance().getX() - 8, HiveMindEmulator.getInstance().getY());
            }
        });
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
