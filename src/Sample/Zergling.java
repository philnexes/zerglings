package Sample;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class Zergling
{
    private static Image img = new Image("zergling.png");
    private ImageView imv1;

    public Zergling()
    {
        this(50, true, true, true, 0, 0);
    }

    public Zergling(double x, double y)
    {
        this(50, true, true, true, x, y);
    }

    public Zergling(int fitWidth)
    {
        this(fitWidth, true, true, true, 0 ,0);
    }

    public Zergling(int fitWidth, double x, double y)
    {
        this(fitWidth, true, true, true, x ,y);
    }

    public Zergling(int fitWidth, boolean preserveRatio, boolean smooth, boolean cache, double x, double y)
    {
        imv1 = new ImageView(img);
        imv1.setFitWidth(fitWidth);
        imv1.setPreserveRatio(preserveRatio);
        imv1.setSmooth(smooth);
        imv1.setCache(cache);
        imv1.setX(x);
        imv1.setY(y);
    }

    public ImageView getImv1()
    {
        return imv1;
    }

    public void setX(double x)
    {
        this.imv1.setX(x);
    }

    public double getX()
    {
        return this.imv1.getX();
    }

    public void setY(double y)
    {
        this.imv1.setY(y);
    }

    public double getY()
    {
        return this.imv1.getY();
    }

    public void setPosition(double x, double y)
    {
        this.imv1.setX(x);
        this.imv1.setY(y);
    }

    public Zergling paint(Pane root)
    {
        root.getChildren().add(this.imv1);

        return this;
    }

    public void erase(Pane root) {root.getChildren().remove(this.imv1);}
}
