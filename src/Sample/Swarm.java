package Sample;

import javafx.animation.AnimationTimer;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.Random;

public class Swarm                              //jinak take pole zergu
{
    private Random rand;
    private int pocet;
    private ArrayList<Zergling> zergs;
    private static int zerg_size = 50;

    public Swarm(int pocet)
    {
        this.pocet = pocet;
        this.zergs = new ArrayList<>();
        rand = new Random();
        for (int i = 0; i < pocet; ++i)
        {
            double x = (double)rand.nextInt(1000);
            double y = (double)rand.nextInt(1000);
            int size = zerg_size - (rand.nextInt(25));
            zergs.add(new Zergling(size, x, y));
        }
    }

    public int getPocet()
    {
        return this.pocet;
    }

    public ArrayList<Zergling> getZergs()
    {
        return zergs;
    }

    public void moveZerglings()
    {
        new AnimationTimer()
        {
            @Override
            public void handle(long now)
            {
                for (Zergling zerg : zergs)
                {
                    if(!HiveMindEmulator.exists())
                        moveNormally(zerg);
                    else
                        runAway(zerg);
                }
            }
        }.start();
    }

    private void moveNormally(Zergling zerg)
    {
        double x = (double)rand.nextInt(8) - 4;
        double y = (double)rand.nextInt(8) - 4;

        int mode = rand.nextInt(125);

        if(mode > 75)
            zerg.setPosition(zerg.getX() + x, zerg.getY() + y);
        else if (mode > 50)
            zerg.setPosition(zerg.getX() - x, zerg.getY() + y);
        else if (mode > 25)
            zerg.setPosition(zerg.getX() + x, zerg.getY() - y);
        else
            zerg.setPosition(zerg.getX() - x, zerg.getY() - y);
    }

    private void runAway(Zergling zerg)
    {
        double x0 = HiveMindEmulator.getInstance().getX() + HiveMindEmulator.getInstance().getWidth()/2 ;
        double y0 = HiveMindEmulator.getInstance().getY() + HiveMindEmulator.getInstance().getHeight()/2;

        double x = x0 - zerg.getX();
        double y = y0 - zerg.getY();

        double vzdalenost = Math.sqrt(x * x + y * y);

        if(vzdalenost < 200)
        {
            if(x >= 0)
            {
                if(y >= 0)
                    zerg.setPosition(zerg.getX() - 2, zerg.getY() - 2);
                else if (y < 0)
                    zerg.setPosition(zerg.getX() - 2, zerg.getY() + 2);
            }
            else if(x < 0)
            {
                if(y > 0)
                    zerg.setPosition(zerg.getX() + 2, zerg.getY() - 2);
                else if (y < 0)
                    zerg.setPosition(zerg.getX() + 2, zerg.getY() + 2);
            }
        }
        else
            moveNormally(zerg);
    }

    public void addZergling(double x, double y, Pane root)
    {
        Zergling z = new Zergling(zerg_size  - (rand.nextInt(25)), x, y);
        zergs.add(z);
        z.paint(root);
    }


    public void paint(Pane root)
    {
        for (Zergling zerg : this.zergs)
        {
            if(!root.getChildren().contains(zerg))
                zerg.paint(root);
        }
    }
}
